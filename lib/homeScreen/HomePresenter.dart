import 'package:benri_frame_data_app/model/Character.dart';
import 'package:benri_frame_data_app/homeScreen/HomeView.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class HomePresenter {
  HomeView _view;

  HomePresenter(this._view);

  Character character;

  Future getPosts() async {
    var firestore = Firestore.instance;
    QuerySnapshot qs = await firestore.collection("characters").getDocuments();
    return qs.documents;
  }
  fetchData() async {
    _view.showLoading();
    QuerySnapshot snapshot =
    await Firestore.instance.collection('Characters').getDocuments();
    if (snapshot.documents.length > 0) {
      List<Character> character = snapshot.documents.map<Character>((document) {
        String name = document['Name'];
        String icon = document['Icon'];
        print(name);
        List<String> airL = new List<String>();
        List<String> airM = new List<String>();
        List<String> lowL = new List<String>();
        List<String> lowH = new List<String>();
        List<String> lowM = new List<String>();
        List<String> qcb = new List<String>();
        List<String> qcf = new List<String>();
        List<String> standH = new List<String>();
        List<String> standL = new List<String>();
        List<String> standM = new List<String>();
        for(int i = 0; i < 6; i++) {
          airL.add(document['AirL'][i]);
          airM.add(document['AirM'][i]);
          lowL.add(document['LowL'][i]);
          lowH.add(document['LowH'][i]);
          lowM.add(document['LowM'][i]);
          qcb.add(document['QCB'][i]);
          qcf.add(document['QCF'][i]);
          standH.add(document['StandH'][i]);
          standL.add(document['StandL'][i]);
          standM.add(document['StandM'][i]);
        }

        return Character(name, icon, airL, airM, standL, standM, standH,
            lowL, lowM, lowH, qcf, qcb);
      }).toList();
      _view.showList(character);
    } else {
      _view.showError();
    }
    _view.hideLoading();
  }
  /*loadCharacters() {
    var equinus = new Character(
        'Shin Equinus',
        'assets/images/equinus.png',
        ["3f", "8f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"]);
    var equinus2 = new Character(
        'Equinus2',
        'assets/images/equinus.png',
        ["9f", "9f", "9f", "9f", "9f", "9f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"]);
    var equinus3 = new Character(
        'Equinus3',
        'assets/images/equinus.png',
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"]);
    var equinus4 = new Character(
        'Equinus4',
        'assets/images/equinus.png',
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"]);
    var equinus5 = new Character(
        'Equinus5',
        'assets/images/equinus.png',
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"]);
    var equinus6 = new Character(
        'Equinus6',
        'assets/images/equinus.png',
        ["6f", "6f", "6f", "6f", "6f", "6f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"]);
    var equinus7 = new Character(
        'Equinus7',
        'assets/images/equinus.png',
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"]);
    var equinus8 = new Character(
        'Equinus8',
        'assets/images/equinus.png',
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"]);
    var equinus9 = new Character(
        'Equinus9',
        'assets/images/equinus.png',
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"],
        ["3f", "3f", "3f", "3f", "3f", "3f"]);

    _view.showList([
      equinus,
      equinus2,
      equinus3,
      equinus4,
      equinus5,
      equinus6,
      equinus7,
      equinus8,
      equinus9
    ]);
    print(equinus.name);
  }*/

  void onClickElement(character) {
    _view.detailScreen(character);
  }
}

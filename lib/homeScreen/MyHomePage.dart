import 'package:benri_frame_data_app/model/Character.dart';
import 'package:benri_frame_data_app/detailScreen/DetailScreen.dart';
import 'package:benri_frame_data_app/homeScreen/HomePresenter.dart';
import 'package:benri_frame_data_app/homeScreen/HomeView.dart';
import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> implements HomeView {
  HomePresenter homePresenter;
  Character character;
  List<Character> characters = [];
  bool _isLoading = true;
  bool _isError = false;
  _MyHomePageState() {
    homePresenter = HomePresenter(this);
  }

  @override
  void initState() {
    // TODO: implement initState
    homePresenter.getPosts();
    //homePresenter.loadCharacters();
    homePresenter.fetchData();

  }

  @override
  void showList(List<Character> characterList) {
    setState(() {
      this.characters = characterList;
    });
  }

  @override
  Widget build(BuildContext context) {
    final title = 'Character List';

    return MaterialApp(
      title: title,
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(182, 137, 0, 1),
          title: Text(title),
        ),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/images/backgroundMainMenu.png")),
          ),
          child: Padding(
            padding:  EdgeInsets.all(13.0),

            child: GridView.count(
              // Create a grid with 2 columns. If you change the scrollDirection to
              // horizontal, this would produce 2 rows.
              crossAxisCount: 2,
              // Generate 100 Widgets that display their index in the List
              children: List.generate(characters.length, (index) {
                return Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[

                      GestureDetector(
                        onTap: () {
                          print("has pulsado " + characters[index].name);
                          homePresenter.onClickElement(characters[index]);
                        },
                        child: Card(
                          elevation: 10.0,
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(90.0))),
                          child: Container(
                            margin: const EdgeInsets.all(0),
                            width: 130.0,
                            height: 130.0,
                            foregroundDecoration: BoxDecoration(
                              border: Border.all(
                                  color: Color.fromARGB(255, 163, 163, 1)),
                              borderRadius:
                                  BorderRadius.all(Radius.elliptical(90, 90)),
                              image: DecorationImage(
                                image: NetworkImage(characters[index].icon),
                                fit: BoxFit.fill
                                  ),
                            ),
                          ),
                        ),
                      ),
                      Text(
                        characters[index].name,
                        style: TextStyle(
                            fontSize: 18, color: Color.fromARGB(255, 0, 0, 1)),
                      ),
                    ],
                  ),
                );
              }),
            ),
          ),
        ),
      ),
    );
  }

  @override
  detailScreen(character) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => DetailScreen(character: character),
      ),
    );
  }

  @override
  hideLoading() {
    setState(() {
      _isLoading = false;
    });
  }

  @override
  showError() {
    setState(() {
      _isError = true;
    });
  }

  @override
  showLoading() {
    setState(() {
      _isLoading = true;
    });
  }
}

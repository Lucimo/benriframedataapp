import 'package:benri_frame_data_app/model/Character.dart';

abstract class HomeView {
  void showList(List<Character> characterList);
  showLoading();
  detailScreen(character);
  hideLoading();
  showError();
}

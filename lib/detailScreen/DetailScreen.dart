import 'package:benri_frame_data_app/model/Character.dart';
import 'package:flutter/material.dart';

class DetailScreen extends StatelessWidget {
  final Character character;
  final List<Entry> moves;

  DetailScreen({Key key, this.character})
      : moves = <Entry>[
          Entry(
            'Ground Moves',
            <Attack>[
              Attack(
                'Stand L:',
                <Data>[
                  Data(character.standL),
                ],
              ),
              Attack(
                'Stand M:',
                <Data>[
                  Data(character.standM),
                ],
              ),
              Attack(
                'Stand H:',
                <Data>[
                  Data(character.standH),
                ],
              ),
              Attack(
                'Low L:',
                <Data>[
                  Data(character.lowL),
                ],
              ),
              Attack(
                'Low M:',
                <Data>[
                  Data(character.lowM),
                ],
              ),
              Attack(
                'Low H:',
                <Data>[
                  Data(character.lowH),
                ],
              ),
            ],
          ),
          Entry(
            'Air Moves',
            <Attack>[
              Attack(
                'Jump L:',
                <Data>[
                  Data(character.standL),
                ],
              ),
              Attack(
                'Jump H:',
                <Data>[
                  Data(character.standH),
                ],
              ),
            ],
          ),
          Entry(
            'Specials',
            <Attack>[
              Attack(
                'QCF:',
                <Data>[
                  Data(character.qcf),
                ],
              ),
              Attack(
                'QCB:',
                <Data>[
                  Data(character.qcb),
                ],
              ),
            ],
          ),
        ];

  // Toda la lista multinivel que muestra esta app.

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(182, 137, 0, 1),
        title: Text(character.name),
      ),
      body: Container(
        padding: EdgeInsets.all(13.0),
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage("assets/images/backgroundMainMenu.png")),
        ),
        child: ListView.builder(
          itemBuilder: (BuildContext context, int index) =>
              EntryItem(moves[index]),
          itemCount: moves.length,
        ),
      ),
    );
  }
}

class Entry {
  Entry(this.title, [this.children = const <Attack>[]]);

  final String title;
  final List<Attack> children;
}

class Attack {
  Attack(this.title, [this.attack = const <Data>[]]);

  final String title;
  final List<Data> attack;
}

class Data {
  Data(this.data);


  final List<String> data;
}

// Muestra una entrada. Si la entrada tiene hijos, se muestra
// con un ExpansionTile.

class EntryItem extends StatelessWidget {
  const EntryItem(this.entry);

  final Entry entry;

  Widget _buildTiles(Entry root) {
    if (root.children.isEmpty) return ListTile(title: Text(root.title));
    return Card(
      elevation: 8,
      color: Color.fromRGBO(255, 255, 255, 1),
      child: ExpansionTile(
        key: PageStorageKey<Entry>(root),
        title: Text(root.title),
        children: root.children.map(_buildAttacks).toList(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildTiles(entry);
  }
  Widget _buildAttacks(Attack root) {
    if (root.attack.isEmpty) return ListTile(title: Text(root.title));
    return Card(
      elevation: 3,
      color: Color.fromRGBO(255, 255, 255, 1),
      child: ExpansionTile(
        key: PageStorageKey<Attack>(root),
        title: Text(root.title),
        children: root.attack.map(_buildData).toList(),
      ),
    );
  }
  Widget _buildData(Data root) {
    return ListTile(
      title: Column(

        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(

                 "DMG:             " + root.data[0],

                style: TextStyle(fontSize: 19, color: Colors.blue),
              ),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                "GUARD:         " + root.data[1],
                style: TextStyle(fontSize: 19, color: Colors.blue),
              ),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                "STARTUP:     " + root.data[2],
                style: TextStyle(fontSize: 19, color: Colors.blue),
              ),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                "ACTIVE:         " + root.data[3],
                style: TextStyle(fontSize: 19, color: Colors.blue),
              ),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                "RECOVERY:  " + root.data[4],
                style: TextStyle(fontSize: 19, color: Colors.blue),
              ),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                "ON BLOCK:   " + root.data[5],
                style: TextStyle(fontSize: 19, color: Colors.blue),
              ),
            ],
          ),
        ],
      ),
      key: PageStorageKey<Data>(root),

    );
  }



  /*Widget _buildAttacks(Attack root) {
    return ListTile(
      title: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            root.title + root.attack.toString(),
            style: TextStyle(fontSize: 19),
          ),
        ],
      ),
      key: PageStorageKey<Attack>(root),

    );
  }
  */
}

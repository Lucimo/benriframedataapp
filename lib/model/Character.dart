class Character {
  String name;
  String icon;
  final List<String> airL;
  final List<String> airM;
  final List<String> standL;
  final List<String> standM;
  final List<String> standH;
  final List<String> lowL;
  final List<String> lowM;
  final List<String> lowH;
  final List<String> qcf;
  final List<String> qcb;

  Character(this.name, this.icon, this.airL, this.airM, this.standL, this.standM, this.standH,
      this.lowL, this.lowM, this.lowH, this.qcf, this.qcb);
}
